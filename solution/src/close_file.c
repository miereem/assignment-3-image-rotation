#include <stdbool.h>
#include <stdio.h>

#include "../include/close_file.h"


bool close_file(FILE** file) {
	return !fclose(*file);
}

