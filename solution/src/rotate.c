#include "../include/rotate.h"

struct image rotate(const struct image *src ) {
    struct image img = create_image(src->height,src->width);
    if (img.pixels == NULL) {
    	return (struct image){0};
    }
    for (size_t i = 0; i < src->height; i++) {
        for (size_t j = 0; j < src->width; j++) {
          img.pixels[src->height*j + src->height-i-1] = src->pixels[src->width*i + j];
        }
    }
    return img;
}
