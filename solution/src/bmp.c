#include "../include/bmp_utils.h"
#include "../include/image.h"

#include <stdio.h>

#define BMP_TYPE 19778
#define BMP_SIZE 40
#define BI_BITCOUNT 24
#define BF_RESERVED 0
#define BI_PLANES 1
#define BI_COMP 0
#define BI_XPELS 2834
#define BI_YPELS 2834
#define BI_USED 0
#define BI_IMPOR 0


static inline size_t get_padding(size_t width) {
	return (4 - (size_t)((width * sizeof(struct pixel)) % 4));
}

static enum read_status get_header(FILE* file, struct bmp_header* header) {
	if (fread(header, sizeof(struct bmp_header), 1, file)) {
		return READ_OK;
	} else {
		return READ_INVALID_HEADER;
	}
}

static enum read_status get_pixels(FILE* file, struct image* img) {
	struct image new = create_image(img->width, img->height);
	if (new.pixels == NULL) return READ_INVALID_BITS; 
	for (size_t i = 0; i < new.height; ++i) {
		if (fread(new.pixels + (i * new.width), sizeof(struct pixel), new.width, file) != new.width) {
			free(new.pixels);
			return READ_INVALID_SIGNATURE;
		}
		if (fseek(file, (int)get_padding(new.width), SEEK_CUR) != 0) {
			free(new.pixels);
			return READ_INVALID_SIGNATURE;
		}
	}
	img->pixels = new.pixels;
	return READ_OK;
}

static struct bmp_header create_header(const struct image* img) {
	uint32_t image_size = (sizeof(struct pixel) * img->width + get_padding(img->width)) * img->height;
	return (struct bmp_header) {
		.bfType = BMP_TYPE,
            	.bfileSize = sizeof(struct bmp_header) + image_size,
            	.bfReserved = BF_RESERVED,
            	.bOffBits = sizeof(struct bmp_header),
            	.biSize = BMP_SIZE,
            	.biWidth = img->width,
            	.biHeight = img->height,
            	.biPlanes = BI_PLANES, 
            	.biBitCount = BI_BITCOUNT,
            	.biCompression = BI_COMP,
            	.biSizeImage = image_size,
            	.biXPelsPerMeter = BI_XPELS,
            	.biYPelsPerMeter = BI_YPELS,
            	.biClrUsed = BI_USED,
            	.biClrImportant = BI_IMPOR
	};
}

static enum write_status write_header(FILE *file, struct bmp_header* header) {
	if (fwrite(header, sizeof(struct bmp_header), 1, file) == 1) {
		return WRITE_OK;
	} else {
		return WRITE_ERROR;
	}
}

static enum write_status write_pixels(FILE* file, const struct image* img) {
	for (size_t i = 0; i < img->height; ++i) {
		if (fwrite(img->pixels + i * img->width, sizeof(struct pixel), img->width, file) != img->width){
			return WRITE_ERROR;
		}
		if (fseek(file, (int)get_padding(img->width), SEEK_CUR) != 0) {
			return WRITE_ERROR;
		}
	}
	return WRITE_OK;
}

enum read_status from_bmp(FILE* in, struct image* img) {
	struct bmp_header header = {0};
	if (get_header(in, &header) == READ_OK) {
		img->width = header.biWidth;
		img->height = header.biHeight;
		return get_pixels(in, img);
	} else {
		return READ_INVALID_SIGNATURE;
	}
}


enum write_status to_bmp(FILE* out, const struct image *img) {
	struct bmp_header header = create_header(img);
	if (write_header(out, &header) == WRITE_OK) {
		return write_pixels(out, img);
	} else {
		return WRITE_ERROR;
	}

}
