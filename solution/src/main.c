#include "../include/bmp_utils.h"
#include "../include/close_file.h"
#include "../include/image.h"
#include "../include/rotate.h"


#include <stdio.h>




int main(int argc, char** argv ) {
	if (argc != 3) {
		fprintf(stderr, "Invalid parameters");
		return 1;
	}
	
	FILE* in = fopen(argv[1], "rb");
	FILE* out = fopen(argv[2], "wb");
	
	if ( in == NULL || out == NULL ) {
		fprintf(stderr, "Couldn't open the file");
		if (in != NULL && !close_file(&in)) {
			fprintf(stderr, "Couldn't close the one pathetic opened file");
		}
		if (out != NULL && !close_file(&out)) {
			fprintf(stderr, "Couldn't close the one pathetic opened file");
		}
		return 1;
	}
	
	

	
	
	struct image img = {0};
	if (from_bmp(in, &img) != READ_OK) {
		fprintf(stderr, "Couldn't read the file");
		destroy_image(&img);
		if (!close_file(&in)  || !close_file(&out)) {
			fprintf(stderr, "Couldn't close the file");
		}
		return 1;
	}
	
	
	
	
	struct image rotated = rotate(&img);
	if (rotated.width != 0 && to_bmp(out, &rotated) != WRITE_OK) {
		fprintf(stderr, "Couldn't write the file");
		destroy_image(&img);
		destroy_image(&rotated);
		if (!close_file(&in)  || !close_file(&out)) {
			fprintf(stderr, "Couldn't close the file");
		}
		return 1;
	}



	if ( !close_file(&in) || !close_file(&out)) {
		fprintf(stderr, "Couldn't close the file");
		destroy_image(&img);
		destroy_image(&rotated);
		return 1;
	}

	destroy_image(&img);
	destroy_image(&rotated);	
    return 0;
}

