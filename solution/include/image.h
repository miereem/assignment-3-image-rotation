#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#pragma once

struct pixel {
    uint8_t b, g, r;
}__attribute__((packed));

struct image {
  uint64_t width, height;
  struct pixel *pixels;
};

void destroy_image(const struct image *img);
struct image create_image(size_t width, size_t height);
